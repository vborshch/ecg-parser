
class ecg_parser:

  #--------------------------------------------------------------------------------
  #
  #--------------------------------------------------------------------------------
  def __init__(self):
    self.pf = 0
    self.bytes_per_acc    = 4
    self.bytes_per_sample = 3
    self.num_channels     = 8
    self.acceler_ena      = 1
    self.ecg_words        = 20
    self.data_size        = self.num_channels * self.ecg_words * self.bytes_per_sample
    self.data_array       = []*self.num_channels
    for i in range(self.num_channels):
      self.data_array.append([]*self.num_channels)
    self.acce_array       = []

  #--------------------------------------------------------------------------------
  #
  #--------------------------------------------------------------------------------
  def divide_chunks(self, dat, size): 
    for i in range(0, len(dat), size):  
      yield dat[i:i + size]

  #--------------------------------------------------------------------------------
  #
  #--------------------------------------------------------------------------------
  def load_settings(self, fname):
    with open(fname, "rb") as f:
      byte = f.read(53)
      self.num_channels = byte[50]
      self.acceler_ena  = byte[51]
      self.ecg_words    = byte[52]
      print("Loaded settings:")
      print("Channels:", self.num_channels, "Accelerometer:", self.acceler_ena, "Words:", self.ecg_words)

  #--------------------------------------------------------------------------------
  #
  #--------------------------------------------------------------------------------
  def proc_data(self, chunk):
    # Convert data to 32 bits numbers
    data    = chunk[:self.data_size]
    numbers = self.divide_chunks(data, 3)
    samples = []
    for x in numbers:
      samples.append(int.from_bytes(x, byteorder='little',signed=True))

    # ------------------------------------------
    # Print debug
    # print('Data raw:\n')
    # for byte in data:
    #   print('%02x ' % (byte), end='')
    # print()
    # print()
    # print('Data decoded:\n')
    # print(samples)
    # print()
    # ------------------------------------------

    # Split to channels
    channel = self.num_channels*[0]
    for i in range(0,self.num_channels):
      channel[i] = samples[i::self.num_channels]
    #
    return channel

  #--------------------------------------------------------------------------------
  #
  #--------------------------------------------------------------------------------
  def proc_acc(self, chunk):
    # Convert accelerometer to 32 bits number
    accl_num = int.from_bytes(chunk, byteorder='little',signed=True)
    # ------------------------------------------
    # print('Accelerometer raw: ', end='')
    # for byte in chunk:
    #   print('%02x ' % (byte), end='')
    # print()
    # print()
    # print('Accelerometer decoded: ', accl_num)
    # ------------------------------------------
    return accl_num

  #--------------------------------------------------------------------------------
  #
  #--------------------------------------------------------------------------------
  def run(self, fname):
    print("Start processing...")
    # Calculate size of one chunk
    self.data_size = self.num_channels * self.ecg_words * self.bytes_per_sample
    with open(fname, "rb") as f:
      while True:
        # Read data chunk
        byte = f.read(self.data_size)
        if not byte:
          break
        # Data processing
        data = self.proc_data(byte)
        # Accelerometer data processing
        if (self.acceler_ena == 1):
          byte = f.read(4)
          if not byte:
            break
          acce = self.proc_acc(byte)
        # Data collect
        for i in range(self.num_channels):
          self.data_array[i] += (data[i])
        self.acce_array.append(acce)
      f.close()
    print("Done!")
    #
    return self.data_array, self.acce_array

  #--------------------------------------------------------------------------------
  #
  #--------------------------------------------------------------------------------
  def save_to_file(self, fname):
    f = open(fname + "_data.dat", "w")
    for i in range(len(self.data_array)):
      for x in self.data_array[i]:
        f.write("%s " % x)
      f.write("\n")
    f.close()

    f = open(fname + "_acc.dat", "w")
    f.write(str(self.acce_array))
    f.close()
